package com.zhidao.backend;

import com.google.gson.reflect.TypeToken;
import javafx.util.Pair;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@SpringBootTest
class ZhidaoBackendApplicationTests {

    @Test
    void contextLoads() {
    }

    @Test
    void testGson() {
        String tags = "[\"java\",\"python\"]";
        Gson gson = new Gson();
        Set<String> tag = gson.fromJson(tags, new TypeToken<Set<String>>(){}.getType());
        System.out.println(tag);
    }

    @Test
    void testSort() {
        List<Pair<Integer, Integer>> list = new ArrayList<>();
        list.add(new Pair<>(1, 2));
        list.add(new Pair<>(2, 3));
        list.add(new Pair<>(3, 4));
        list.add(new Pair<>(4, 5));
        list.add(new Pair<>(5, 6));
        list.stream().sorted((a, b) -> (b.getValue() - a.getValue())).limit(3).forEach(System.out::println);
    }

}
