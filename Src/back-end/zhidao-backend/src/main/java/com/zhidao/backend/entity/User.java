package com.zhidao.backend.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.sql.Timestamp;
import java.util.List;

/**
 * 用户实体类，对应数据库表
 */
@TableName("user")
@Data
public class User {
    /**
     * 主键id
     */
    @TableId(type = IdType.AUTO)
    private Long id;
    /**
     * 用户名
     */
    private String username;
    /**
     * 用户邮箱
     */
    private String email;
    /**
     * 密码（加密存储）
     */
    private String password;
    /**
     * 学历
     */
    private String education;
    /**
     * 头像URL
     */
    private String avatar;
    /**
     * 个人描述
     */
    private String description;
    /**
     * 积分
     */
    private Integer score;
    /**
     * 签到天数
     */
    private Integer days;
    /**
     * 收藏数量
     */
    private Integer collect;
    /**
     * 订阅数量
     */
    private Integer subscribe;
    /**
     * 粉丝数量
     */
    private Integer followers;
    /**
     * 发布路线数量
     */
    private Integer routes;
    /**
     * 学习路线数量
     */
    private Integer learn;
    /**
     * json格式存储的标签，可以少用一个表
     */
    @TableField("tags")
    private String tagStr;

    @TableField(exist = false)
    private List<String> tags;
    /**
     * 创建日期
     */
    @TableField("created_date")
    private Timestamp createdDate;
    /**
     * 是否会员
     */
    private Boolean vip;
    /**
     * 是否删除
     */
    @TableLogic
    private Boolean deleted;
    /**
     * 是否管理员
     */
    private Boolean admin;
    /**
     * 是否封号
     */
    private Boolean banned;
}
