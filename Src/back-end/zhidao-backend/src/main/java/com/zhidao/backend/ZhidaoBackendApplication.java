package com.zhidao.backend;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
@MapperScan("com.zhidao.backend.mapper")
public class ZhidaoBackendApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(ZhidaoBackendApplication.class, args);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(ZhidaoBackendApplication.class);
    }

}
