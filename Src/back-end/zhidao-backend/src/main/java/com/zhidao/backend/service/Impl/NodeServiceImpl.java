package com.zhidao.backend.service.Impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zhidao.backend.entity.Node;
import com.zhidao.backend.mapper.NodeMapper;
import com.zhidao.backend.service.INodeService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NodeServiceImpl extends ServiceImpl<NodeMapper, Node> implements INodeService {
    @Override
    public Long publishNode(Node node, Long routeId) {
        node.setRouteId(routeId);
        node.setDeleted(false);
        this.baseMapper.insert(node);
        return node.getId();
    }

    @Override
    public List<Node> getNodesByRouteId(Long routeId) {
        return this.baseMapper.selectList(new QueryWrapper<Node>().eq("route_id", routeId).orderByAsc("position"));
    }
}
