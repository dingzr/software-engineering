package com.zhidao.backend.entity;

import lombok.Data;

@Data
public class RouteLearnProgress {

    private Route route;

    private String authorName;

    private Integer progress;
}
