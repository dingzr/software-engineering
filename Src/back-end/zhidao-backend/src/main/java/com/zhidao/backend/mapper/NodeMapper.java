package com.zhidao.backend.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zhidao.backend.entity.Node;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface NodeMapper extends BaseMapper<Node> {
}
