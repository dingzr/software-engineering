package com.zhidao.backend.common;

import com.zhidao.backend.entity.Route;
import lombok.Data;

@Data
public class PublishRequest {

    private Long id;

    private Boolean isDraft;

    private Route route;
}
