package com.zhidao.backend.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zhidao.backend.entity.Collect;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface CollectMapper extends BaseMapper<Collect> {
}
