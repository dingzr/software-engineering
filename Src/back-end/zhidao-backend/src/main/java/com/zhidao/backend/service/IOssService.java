package com.zhidao.backend.service;


import org.springframework.web.multipart.MultipartFile;


/**
 * 对象存储服务
 */
public interface IOssService {

    String uploadImage(MultipartFile file);
}
