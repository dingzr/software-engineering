package com.zhidao.backend.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.sql.Timestamp;
import java.util.List;

@TableName("comment")
@Data
public class Comment {
    @TableId(type = IdType.AUTO)
    private Long id;
    @TableField("route_id")
    private Long routeId;
    @TableField("user_id")
    private Long userId;
    @TableField(exist = false)
    private String username;
    @TableField(exist = false)
    private String avatar;
    private Integer floor;
    @TableField("comment_lines")
    private String commentLines;
    @TableField("comment_time")
    private Timestamp commentTime;
    @TableLogic
    private Boolean deleted;
    @TableField(exist = false)
    private List<SubComment> subComments;
}
