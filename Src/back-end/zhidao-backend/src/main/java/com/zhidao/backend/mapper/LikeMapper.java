package com.zhidao.backend.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zhidao.backend.entity.Like;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface LikeMapper extends BaseMapper<Like> {
}
