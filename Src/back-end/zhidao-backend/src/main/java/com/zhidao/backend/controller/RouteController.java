package com.zhidao.backend.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.zhidao.backend.annotation.LoginToken;
import com.zhidao.backend.annotation.PassToken;
import com.zhidao.backend.common.*;
import com.zhidao.backend.entity.*;
import com.zhidao.backend.exception.BusinessException;
import com.zhidao.backend.service.*;
import com.zhidao.backend.util.JwtUtil;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 路线控制器
 */
@RestController
@RequestMapping("/route")
public class RouteController {

    @Resource
    private IUserService userService;

    @Resource
    private IRouteService routeService;

    @Resource
    private IOssService ossService;

    @Resource
    private INodeService nodeService;

    @Resource
    private IResourceService resourceService;

    @Resource
    private ICollectService collectService;

    @Resource
    private ILearnService learnService;

    @Resource
    private ICommentService commentService;

    @Resource
    private ISubCommentService subCommentService;

    @Resource
    private ILikeService likeService;

    /**
     * 根据单个关键词搜索路线
     * @param keyword : 关键词
     * @return : 搜索结果
     */
    @PassToken
    @GetMapping("/search")
    public Result<List<Route>> searchRoute(@RequestParam(name = "keyword") String keyword) {
        if (StringUtils.isBlank(keyword)) {
            throw new BusinessException(StatusCode.PARAM_ERROR.getCode(), "请求参数为空");
        }
        Gson gson = new Gson();
        List<Route> routes = routeService.search(keyword);
        routes.forEach(route -> route.setTags(gson.fromJson(route.getTagStr(), new TypeToken<List<String>>(){}.getType())));
        List<Route> finalRoutes = routes.stream().peek(route -> route.setAuthorName(userService.getById(route.getAuthor()).getUsername())).collect(Collectors.toList());
        return Result.success(finalRoutes);
    }

    /**
     * 获取路线详情
     * @param id : 路线id
     * @return : 请求结果
     */
    @PassToken
    @GetMapping("/detail")
    public Result<DetailRoute> getRouteDetail(@RequestParam(name = "id") Long id, HttpServletRequest request) {
        if (id == null) {
            throw new BusinessException(StatusCode.PARAM_ERROR.getCode(), "请求参数为空");
        }
        List<Node> nodes = nodeService.getNodesByRouteId(id);
        nodes.forEach(node -> node.setResources(resourceService.getResourcesByNodeId(node.getId())));
        Route route = routeService.getById(id);
        route.setTags(new Gson().fromJson(route.getTagStr(), new TypeToken<List<String>>(){}.getType()));
        route.setNodes(nodes);
        DetailRoute detailRoute = new DetailRoute();
        detailRoute.setRoute(route);
        String token = request.getHeader("Authorization");
        if (StringUtils.isBlank(token)) {
            detailRoute.setCollected(false);
            detailRoute.setLearned(false);
            detailRoute.setLiked(false);
            detailRoute.setProgress(0);
        } else {
            Long userId = JwtUtil.getUserId(token);
            detailRoute.setCollected(collectService.isCollect(userId, id));
            detailRoute.setLearned(learnService.isLearn(userId, id));
            if (detailRoute.getLearned()) {

                detailRoute.setProgress(learnService.getOne(new QueryWrapper<Learn>().eq("user_id", userId).eq("route_id", id)).getProgress());
            } else {
                detailRoute.setProgress(0);
            }
            detailRoute.setLiked(likeService.isLike(userId, id));
        }
        return Result.success(detailRoute);
    }

    /**
     * 获取节点详情
     * @param id : 节点id
     * @return : 请求结果
     */
    @PassToken
    @GetMapping("/node/detail")
    public Result<Node> getNodeDetail(@RequestParam(name = "id") Long id) {
        if (id == null) {
            throw new BusinessException(StatusCode.PARAM_ERROR.getCode(), "请求参数为空");
        }
        Node node = nodeService.getById(id);
        List<com.zhidao.backend.entity.Resource> resources = resourceService.getResourcesByNodeId(id);
        node.setResources(resources);
        return Result.success(node);
    }

    /**
     * 获取用户发布的路线
     * @param id : 用户id
     * @return : 请求结果
     */
    @LoginToken
    @GetMapping("/user")
    public Result<List<Route>> getUserRoute(@RequestParam(name = "id") Long id) {
        if (id == null) {
            throw new BusinessException(StatusCode.PARAM_ERROR.getCode(), "请求参数为空");
        }
        List<Route> routes = routeService.getRoutes(id);
        return Result.success(routes);
    }

    /**
     * 获取草稿
     * @param id : 用户id
     * @return : 请求结果
     */
    @LoginToken
    @GetMapping("/user/drafts")
    public Result<List<Route>> getUserDrafts(@RequestParam(name = "id") Long id) {
        if (id == null) {
            throw new BusinessException(StatusCode.PARAM_ERROR.getCode(), "请求参数为空");
        }
        List<Route> routes = routeService.getDrafts(id);
        return Result.success(routes);
    }

    /**
     * 获取用户待审核的路线
     * @param id : 用户id
     * @return : 请求结果
     */
    @LoginToken
    @GetMapping("/user/audit")
    public Result<List<Route>> getUserAudit(@RequestParam(name = "id") Long id) {
        if (id == null) {
            throw new BusinessException(StatusCode.PARAM_ERROR.getCode(), "请求参数为空");
        }
        List<Route> routes = routeService.getAuditingRoutes(id);
        return Result.success(routes);
    }

    /**
     * 获取用户已退回的路线
     * @param id : 用户id
     * @return : 请求结果
     */
    @LoginToken
    @GetMapping("/user/return")
    public Result<List<Route>> getUserReturn(@RequestParam(name = "id") Long id) {
        if (id == null) {
            throw new BusinessException(StatusCode.PARAM_ERROR.getCode(), "请求参数为空");
        }
        List<Route> routes = routeService.getReturnRoutes(id);
        return Result.success(routes);
    }

    /**
     * 获取用户收藏的路线
     * @param id : 用户id
     * @return : 请求结果
     */
    @LoginToken
    @GetMapping("/user/collect")
    public Result<List<Route>> getUserCollectRoute(@RequestParam(name = "id") Long id) {
        if (id == null) {
            throw new BusinessException(StatusCode.PARAM_ERROR.getCode(), "请求参数为空");
        }
        List<Long> routesId = collectService.getCollectRoutes(id);
        List<Route> routes = routeService.getRoutesById(routesId);
        List<Route> finalRoutes = routes.stream().peek(route -> {
            route.setTags(new Gson().fromJson(route.getTagStr(), new TypeToken<List<String>>() {}.getType()));
            route.setAuthorName(userService.getById(route.getAuthor()).getUsername());
        }).collect(Collectors.toList());
        return Result.success(finalRoutes);
    }

    /**
     * 获取用户学习的路线
     * @param id : 用户id
     * @return : 请求结果
     */
    @LoginToken
    @GetMapping("/user/learn")
    public Result<List<RouteLearnProgress>> getUserLearnRoute(@RequestParam(name = "id") Long id) {
        if (id == null) {
            throw new BusinessException(StatusCode.PARAM_ERROR.getCode(), "请求参数为空");
        }
        Map<Long, Integer> routesIdMap = learnService.getLearnRoutes(id);
        if (routesIdMap == null || routesIdMap.size() == 0) {
            return Result.success(new ArrayList<>());
        }
        List<Long> routesId = new ArrayList<>();
        routesIdMap.forEach((k, v) -> routesId.add(k));
        List<Route> routes = routeService.getRoutesById(new ArrayList<>(routesId));
        routes.forEach(route -> route.setNodes(nodeService.getNodesByRouteId(route.getId())));
        Map<Route, Integer> routesMap = routes.stream().collect(Collectors.toMap(route -> route, route -> routesIdMap.get(route.getId())));
        List<RouteLearnProgress> routeLearnProgresses = new ArrayList<>();
        routesMap.forEach((route, progress) -> {
            RouteLearnProgress routeLearnProgress = new RouteLearnProgress();
            routeLearnProgress.setRoute(route);
            routeLearnProgress.setAuthorName(userService.getById(route.getAuthor()).getUsername());
            routeLearnProgress.setProgress(progress);
            routeLearnProgresses.add(routeLearnProgress);
        });
        return Result.success(routeLearnProgresses);
    }

    /**
     * 上传图片
     * @param uploadImageRequest : 上传图片请求
     * @return : 请求结果
     */
    @LoginToken
    @PostMapping("/upload/image")
    public Result<String> uploadImage(UploadImageRequest uploadImageRequest) {
        if (uploadImageRequest == null) {
            throw new BusinessException(StatusCode.PARAM_ERROR.getCode(), "请求参数为空");
        }
        String url = ossService.uploadImage(uploadImageRequest.getFile());
        return Result.success(url);
    }

    /**
     * 点赞路线
     * @param userId : 用户id
     * @param routeId : 路线id
     * @return : 请求结果
     */
    @LoginToken
    @GetMapping("/like")
    public Result<?> likeRoute(@RequestParam(name = "userId") Long userId, @RequestParam(name = "routeId") Long routeId) {
        if (userId == null || routeId == null) {
            throw new BusinessException(StatusCode.PARAM_ERROR.getCode(), "请求参数为空");
        }
        userService.likeRoute(userId);
        routeService.likeRoute(routeId);
        likeService.like(userId, routeId);
        return Result.success();
    }

    /**
     * 取消点赞路线
     * @param userId : 用户id
     * @param routeId : 路线id
     * @return : 请求结果
     */
    @LoginToken
    @GetMapping("/unlike")
    public Result<?> unlikeRoute(@RequestParam(name = "userId") Long userId, @RequestParam(name = "routeId") Long routeId) {
        if (userId == null || routeId == null) {
            throw new BusinessException(StatusCode.PARAM_ERROR.getCode(), "请求参数为空");
        }
        userService.unlikeRoute(userId);
        routeService.unlikeRoute(routeId);
        likeService.unLike(userId, routeId);
        return Result.success();
    }

    /**
     * 收藏路线
     * @param userId : 用户id
     * @param routeId : 路线id
     * @return : 请求结果
     */
    @LoginToken
    @GetMapping("/collect")
    public Result<?> collectRoute(@RequestParam(name = "userId") Long userId, @RequestParam(name = "routeId") Long routeId) {
        if (userId == null || routeId == null) {
            throw new BusinessException(StatusCode.PARAM_ERROR.getCode(), "请求参数为空");
        }
        userService.collectRoute(userId);
        routeService.collectRoute(routeId);
        collectService.collect(userId, routeId);
        return Result.success();
    }

    /**
     * 取消收藏路线
     * @param userId : 用户id
     * @param routeId : 路线id
     * @return : 请求结果
     */
    @LoginToken
    @GetMapping("/cancel/collect")
    public Result<?> cancelCollectRoute(@RequestParam(name = "userId") Long userId, @RequestParam(name = "routeId") Long routeId) {
        if (userId == null || routeId == null) {
            throw new BusinessException(StatusCode.PARAM_ERROR.getCode(), "请求参数为空");
        }
        userService.cancelCollectRoute(userId);
        routeService.cancelCollectRoute(routeId);
        collectService.cancelCollect(userId, routeId);
        return Result.success();
    }

    /**
     * 学习路线
     * @param userId : 用户id
     * @param routeId : 路线id
     * @return : 请求结果
     */
    @LoginToken
    @GetMapping("/learn")
    public Result<?> learnRoute(@RequestParam(name = "userId") Long userId, @RequestParam(name = "routeId") Long routeId) {
        if (userId == null || routeId == null) {
            throw new BusinessException(StatusCode.PARAM_ERROR.getCode(), "请求参数为空");
        }
        userService.learnRoute(userId);
        routeService.learnRoute(routeId);
        learnService.learnRoute(userId, routeId);
        return Result.success();
    }

    /**
     * 取消学习路线
     * @param userId : 用户id
     * @param routeId : 路线id
     * @return : 请求结果
     */
    @LoginToken
    @GetMapping("/cancel/learn")
    public Result<?> cancelLearnRoute(@RequestParam(name = "userId") Long userId, @RequestParam(name = "routeId") Long routeId) {
        if (userId == null || routeId == null) {
            throw new BusinessException(StatusCode.PARAM_ERROR.getCode(), "请求参数为空");
        }
        userService.cancelLearnRoute(userId);
        routeService.cancelLearnRoute(routeId);
        learnService.cancelLearnRoute(userId, routeId);
        return Result.success();
    }

    /**
     * 更新学习进度
     * @param userId : 用户id
     * @param routeId : 路线id
     * @param progress : 进度
     * @return : 请求结果
     */
    @LoginToken
    @GetMapping("/update/progress")
    public Result<?> updateProgress(@RequestParam(name = "userId") Long userId, @RequestParam(name = "routeId") Long routeId, @RequestParam(name = "progress") Integer progress) {
        if (userId == null || routeId == null || progress == null) {
            throw new BusinessException(StatusCode.PARAM_ERROR.getCode(), "请求参数为空");
        }
        learnService.updateProgress(userId, routeId, progress);
        return Result.success();
    }

    /**
     * 发布路线
     * @param publishRequest : 发布请求
     * @return : 请求结果
     */
    @LoginToken
    @PostMapping("/publish")
    public Result<?> publishRoute(@RequestBody PublishRequest publishRequest) {
        if (publishRequest == null) {
            throw new BusinessException(StatusCode.PARAM_ERROR.getCode(), "请求参数为空");
        }
        if (publishRequest.getRoute().getId() == null) {
            Long routeId = routeService.publishRoute(publishRequest);
            User user = userService.getById(publishRequest.getId());
            user.setRoutes(user.getRoutes() + 1);
            userService.updateById(user);
            Route route = publishRequest.getRoute();
            route.setId(routeId);
            List<Node> nodes = route.getNodes();
            nodes.forEach(node -> {
                node.setRouteId(routeId);
                Long nodeId = nodeService.publishNode(node, routeId);
                List<com.zhidao.backend.entity.Resource> resources = node.getResources();
                resources.forEach(resource -> {
                    resource.setNodeId(nodeId);
                    resource.setDeleted(false);
                });
                resourceService.saveBatch(resources);
            });
        } else {
            publishRequest.getRoute().setTagStr(new Gson().toJson(publishRequest.getRoute().getTags()));
            if (publishRequest.getIsDraft()) {
                publishRequest.getRoute().setStatus(1);
            } else {
                publishRequest.getRoute().setStatus(2);
            }
            routeService.updateById(publishRequest.getRoute());
            Long routeId = publishRequest.getRoute().getId();
            List<Node> nodes = publishRequest.getRoute().getNodes();
            nodes.forEach(node -> {
                if (node.getId() == null) {
                    node.setRouteId(routeId);
                    Long nodeId = nodeService.publishNode(node, routeId);
                    List<com.zhidao.backend.entity.Resource> resources = node.getResources();
                    resources.forEach(resource -> {
                        resource.setNodeId(nodeId);
                        resource.setDeleted(false);
                    });
                    resourceService.saveBatch(resources);
                } else {
                    nodeService.updateById(node);
                    List<com.zhidao.backend.entity.Resource> resources = node.getResources();
                    resources.forEach(resource -> {
                        if (resource.getId() == null) {
                            resource.setNodeId(node.getId());
                            resource.setDeleted(false);
                            resourceService.save(resource);
                        } else {
                            resourceService.updateById(resource);
                        }
                    });
                }
            });
        }
        return Result.success();
    }

    /**
     * 首页推荐路线
     * @return : 请求结果
     */
    @PassToken
    @GetMapping("/recommend")
    public Result<List<Route>> recommendRoute(@NotNull HttpServletRequest request) {
        String token = request.getHeader("token");
        if (StringUtils.isBlank(token) || !JwtUtil.verifyToken(token)) {
            return Result.success(routeService.randomRoutes());
        }
        Long id = JwtUtil.getUserId(token);
        User user = userService.getById(id);
        String tagStr = user.getTagStr();
        return Result.success(routeService.personalizedRoutes(tagStr));
    }

    /**
     * 首页轮播图创作推广
     * @return : 请求结果
     */
    @PassToken
    @GetMapping("/carousel")
    public Result<List<Route>> carouselRoute() {
        return Result.success(routeService.carouselRoutes());
    }

    /**
     * 审核通过路线
     * @param  userId: 用户id
     * @param routeId : 路线id
     * @return : 请求结果
     */
    @LoginToken
    @GetMapping("/pass")
    public Result<?> PassRoute(@RequestParam(name = "userId") Long userId, @RequestParam(name = "routeId") Long routeId) {
        if (userId == null || routeId == null) {
            throw new BusinessException(StatusCode.PARAM_ERROR.getCode(), "请求参数为空");
        }
        Route route = routeService.getById(routeId);
        if(route.getStatus()!=2) {
            throw new BusinessException(StatusCode.PARAM_ERROR.getCode(), "该路线不需要审核");
        }
        if(userService.isAdministrator(userId)) {
            route.setStatus(3);
            routeService.updateById(route);
            return Result.success();
        } else {
            throw new BusinessException(StatusCode.REJECT.getCode(), "您不是管理员");
        }
    }

    /**
     * 审核退回路线
     * @param userId : 用户id
     * @param routeId : 路线id
     * @return : 请求结果
     */
    @LoginToken
    @GetMapping("/return")
    public Result<?> returnRoute(@RequestParam(name = "userId") Long userId, @RequestParam(name = "routeId") Long routeId) {
        if (userId == null || routeId == null) {
            throw new BusinessException(StatusCode.PARAM_ERROR.getCode(), "请求参数为空");
        }
        Route route = routeService.getById(routeId);
        if(route.getStatus()!=2) {
            throw new BusinessException(StatusCode.PARAM_ERROR.getCode(), "该路线不需要审核");
        }
        //用户自己想修改可以退回审核，管理员也可以不给通过而退回
        if(userId.equals(route.getAuthor())) {
            route.setStatus(1);
            routeService.updateById(route);
            return Result.success();
        }
        if(userService.isAdministrator(userId)) {
            route.setStatus(4);
            routeService.updateById(route);
            return Result.success();
        }
        throw new BusinessException(StatusCode.REJECT.getCode(), "您不是管理员");

    }
    /**
     * 用户提出再次编辑的请求，切换为临时保存态
     * @param  userId: 用户id
     * @param routeId : 路线id
     * @return : 请求结果
     */
    @LoginToken
    @GetMapping("/rewrite")
    public Result<?> rewriteRoute(@RequestParam(name = "userId") Long userId, @RequestParam(name = "routeId") Long routeId) {
        if (userId == null || routeId == null) {
            throw new BusinessException(StatusCode.PARAM_ERROR.getCode(), "请求参数为空");
        }
        Route route = routeService.getById(routeId);
        if(route==null) {
            throw new BusinessException(StatusCode.PARAM_ERROR.getCode(), "该路线不存在");
        }
        if(userId.equals(route.getAuthor())) {
            route.setStatus(1);
            routeService.updateById(route);
            return Result.success();
        }
        throw new BusinessException(StatusCode.REJECT.getCode(), "您不是该路线的作者");

    }
    /**
     * 用户删除路线
     * @param  userId: 用户id
     * @param routeId : 路线id
     * @return : 请求结果
     */
    @LoginToken
    @GetMapping("/delete")
    public Result<?> deleteRoute(@RequestParam(name = "userId") Long userId, @RequestParam(name = "routeId") Long routeId) {
        if (userId == null || routeId == null) {
            throw new BusinessException(StatusCode.PARAM_ERROR.getCode(), "请求参数为空");
        }
        Route route = routeService.getById(routeId);
        if(route==null) {
            throw new BusinessException(StatusCode.PARAM_ERROR.getCode(), "该路线不存在");
        }
        //自己或者管理员删除
        if(userId.equals(route.getAuthor())||userService.isAdministrator(userId)) {
            routeService.deleteById(route);
            return Result.success();
        } else {
            throw new BusinessException(StatusCode.REJECT.getCode(), "您不是该路线的作者或管理员");
        }
    }
    /**
     * 管理员界面，待审核路线列表
     * @return : 请求结果
     */
    @LoginToken
    @GetMapping("/checkPendingList")
    public Result<List<Route>> checkPendingRoutes(@NotNull HttpServletRequest request) {
        String token = request.getHeader("Authorization");
        Long id = JwtUtil.getUserId(token);
        User user = userService.getById(id);
        if(user.getAdmin()) {
            List<Route> routes = routeService.getCheckPendingRoutes();
            List<Route> finalRoutes = routes.stream().peek(route -> route.setAuthorName(userService.getById(route.getAuthor()).getUsername())).collect(Collectors.toList());
            return Result.success(finalRoutes);
        } else {
            throw new BusinessException(StatusCode.REJECT.getCode(), "您不是管理员");
        }
    }

    /**
     * 管理员界面，已审核路线列表
     * @return : 请求结果
     */
    @LoginToken
    @GetMapping("/checkPassedList")
    public Result<List<Route>> checkPassedRoutes(@NotNull HttpServletRequest request) {
        String token = request.getHeader("Authorization");
        Long id = JwtUtil.getUserId(token);
        User user = userService.getById(id);
        if(user.getAdmin()) {
            List<Route> routes = routeService.getCheckPassedRoutes();
            List<Route> finalRoutes = routes.stream().peek(route -> route.setAuthorName(userService.getById(route.getAuthor()).getUsername())).collect(Collectors.toList());
            return Result.success(finalRoutes);
        } else {
            throw new BusinessException(StatusCode.REJECT.getCode(), "您不是管理员");
        }
    }

    /**
     * 创作推广
     * @param routId : 路线id
     * @param request : 请求
     * @return : 请求结果
     */
    @LoginToken
    @GetMapping("/popularize")
    public Result<?> popularizeRoute(@RequestParam(name = "routeId") Long routId, HttpServletRequest request) {
        String token = request.getHeader("Authorization");
        Long id = JwtUtil.getUserId(token);
        User user = userService.getById(id);
        if(user.getAdmin()) {
            Route route = routeService.getById(routId);
            if(route==null) {
                throw new BusinessException(StatusCode.PARAM_ERROR.getCode(), "该路线不存在");
            }
            route.setRecommend(true);
            routeService.updateById(route);
            return Result.success();
        } else {
            throw new BusinessException(StatusCode.REJECT.getCode(), "您不是管理员");
        }
    }

    /**
     * 取消创作推广
     * @param routId : 路线id
     * @param request : 请求
     * @return : 请求结果
     */
    @LoginToken
    @GetMapping("/cancelPopularize")
    public Result<?> cancelPopularizeRoute(@RequestParam(name = "routeId") Long routId, HttpServletRequest request) {
        String token = request.getHeader("Authorization");
        Long id = JwtUtil.getUserId(token);
        User user = userService.getById(id);
        if(user.getAdmin()) {
            Route route = routeService.getById(routId);
            if(route==null) {
                throw new BusinessException(StatusCode.PARAM_ERROR.getCode(), "该路线不存在");
            }
            route.setRecommend(false);
            routeService.updateById(route);
            return Result.success();
        } else {
            throw new BusinessException(StatusCode.REJECT.getCode(), "您不是管理员");
        }
    }
    /**
     * 展示路线的评论
     * @param routeId : 路线id
     * @return : 请求结果
     */
    @PassToken
    @GetMapping("/show/comment")
    public Result<List<Comment>> showCommentList(@RequestParam(name = "routeId") Long routeId) {
        List<Comment> commentList=commentService.getCommentList(routeId);
        commentList.forEach(comment -> {
            comment.setUsername(userService.getUsernameById(comment.getUserId()));
            comment.setAvatar(userService.getUserInfo(comment.getUserId()).getAvatar());
            List<SubComment> subCommentList = subCommentService.getSubCommentList(comment.getId());
            subCommentList.forEach(subComment -> {
                subComment.setUsername(userService.getUsernameById(subComment.getCId()));
                subComment.setAvatar(userService.getUserInfo(subComment.getCId()).getAvatar());
            });
            comment.setSubComments(subCommentList);
        });
        return Result.success(commentList);
    }
    /**
     * 展示楼中楼
     * @param commentId : 路线id
     * @return : 请求结果
     */
    @PassToken
    @GetMapping("/show/subcomment")
    public Result<List<SubComment>> showSubCommentList(@RequestParam(name = "commentId") Long commentId) {
        List<SubComment> subCommentList=subCommentService.getSubCommentList(commentId);
        subCommentList.forEach(subComment -> {
            subComment.setUsername(userService.getUsernameById(subComment.getCId()));
            subComment.setAvatar(userService.getUserInfo(subComment.getCId()).getAvatar());
        });
        return Result.success(subCommentList);
    }
}
