package com.zhidao.backend.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zhidao.backend.entity.Node;

import java.util.List;


public interface INodeService extends IService<Node> {

    Long publishNode(Node node, Long routeId);

    List<Node> getNodesByRouteId(Long routeId);
}
