package com.zhidao.backend.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zhidao.backend.entity.Learn;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface LearnMapper extends BaseMapper<Learn> {
}
