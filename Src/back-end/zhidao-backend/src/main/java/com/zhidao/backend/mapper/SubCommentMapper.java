package com.zhidao.backend.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zhidao.backend.entity.SubComment;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SubCommentMapper extends BaseMapper<SubComment> {
}
