package com.zhidao.backend.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@TableName("resource")
@Data
public class Resource {

    @TableId(type = IdType.AUTO)
    private Long id;

    private String title;

    private String url;

    @TableField("node_id")
    private Long nodeId;

    private Boolean deleted;
}
