package com.zhidao.backend.service.Impl;

import com.zhidao.backend.service.IVerifyService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Random;

@Service
public class VerifyServiceImpl implements IVerifyService {
    @Value("${spring.mail.username}")
    private String from;
    @Resource
    private JavaMailSender mailSender;

    @Override
    public Integer sendSimpleMail(String to) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom(from);
        message.setTo(to);
        message.setSubject("知道平台，注册验证码");
        Random random = new Random();
        int code = random.nextInt(89999) + 10000;
        message.setText("您的验证码是：" + code + "。若不是本人操作请忽略");
        mailSender.send(message);
        return code;
    }
}
