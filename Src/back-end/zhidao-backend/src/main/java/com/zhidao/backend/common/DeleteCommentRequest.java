package com.zhidao.backend.common;

import lombok.Data;

@Data
public class DeleteCommentRequest {
    private Long userId;
    private Long commentId;
}
