package com.zhidao.backend.common;

import lombok.Data;

@Data
public class DeleteSubCommentRequest {
    private Long userId;
    private Long subCommentId;
}
