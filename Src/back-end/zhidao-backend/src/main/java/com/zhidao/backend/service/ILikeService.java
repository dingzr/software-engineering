package com.zhidao.backend.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zhidao.backend.entity.Like;

public interface ILikeService extends IService<Like> {

    void like(Long userId, Long routeId);

    void unLike(Long userId, Long routeId);

    Boolean isLike(Long userId, Long routeId);
}
