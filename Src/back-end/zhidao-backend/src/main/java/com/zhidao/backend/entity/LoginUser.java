package com.zhidao.backend.entity;

import lombok.Data;

import java.util.List;

/**
 * 用户实体类，对应已登录用户的登录信息
 */
@Data
public class LoginUser {
    private Long id;
    private String token;
    private User user;
    private List<Long> followedIds;
}
