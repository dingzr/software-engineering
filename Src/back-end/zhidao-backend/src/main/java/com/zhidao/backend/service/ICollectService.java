package com.zhidao.backend.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zhidao.backend.entity.Collect;

import java.util.List;

public interface ICollectService extends IService<Collect> {

    void collect(Long userId, Long routeId);

    void cancelCollect(Long userId, Long routeId);

    List<Long> getCollectRoutes(Long id);

    Boolean isCollect(Long userId, Long routeId);
}
