package com.zhidao.backend.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zhidao.backend.entity.Learn;

import java.util.Map;

public interface ILearnService extends IService<Learn> {

    void learnRoute(Long userId, Long routeId);

    void cancelLearnRoute(Long userId, Long routeId);

    void updateProgress(Long userId, Long routeId, Integer progress);

    Map<Long, Integer> getLearnRoutes(Long id);

    Boolean isLearn(Long userId, Long routeId);
}
