package com.zhidao.backend.service.Impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zhidao.backend.entity.Like;
import com.zhidao.backend.mapper.LikeMapper;
import com.zhidao.backend.service.ILikeService;
import org.springframework.stereotype.Service;

@Service
public class LikeServiceImpl extends ServiceImpl<LikeMapper, Like> implements ILikeService {

    @Override
    public void like(Long userId, Long routeId) {
        Like like = new Like();
        like.setUserId(userId);
        like.setRouteId(routeId);
        like.setDeleted(false);
        this.save(like);
    }

    @Override
    public void unLike(Long userId, Long routeId) {
        this.remove(new QueryWrapper<Like>().eq("user_id", userId).eq("route_id", routeId));
    }

    @Override
    public Boolean isLike(Long userId, Long routeId) {
        Like like = this.getOne(new QueryWrapper<Like>().eq("user_id", userId).eq("route_id", routeId));
        return like != null;
    }

}
