package com.zhidao.backend.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.sql.Timestamp;
import java.util.List;

@TableName("route")
@Data
public class Route {
    /**
     * 主键id
     */
    @TableId(type = IdType.AUTO)
    private Long id;
    /**
     * 标题
     */
    private String title;
    /**
     * 描述
     */
    private String description;
    /**
     * 封面链接
     */
    private String cover;
    /**
     * 查看数
     */
    private Integer views;
    /**
     * 点赞数
     */
    private Integer likes;
    /**
     * 收藏数
     */
    private Integer collects;
    /**
     * 作者id
     */
    private Long author;

    /**
     * 作者名
     */
    @TableField(exist = false)
    private String authorName;
    /**
     * 发布时间
     */
    @TableField("post_time")
    private Timestamp postTime;
    /**
     * 标签的JSON格式
     */
    @TableField("tags")
    private String tagStr;
    @TableField(exist = false)
    private List<String> tags;
    /**
     * 是否为精品
     */
    private Boolean recommend;
    /**
     * 当前路线状态：
     *      临时保存（1）
     *      待审核（2）
     *      已发布（3）
     *      已退回（4）   如果不发消息，就多加这么一个状态，用户点击已退回的稿件就再变成临时保存1的状态
     */
    private Integer status ;
    /**
     * 是否删除
     */
    @TableLogic
    private Boolean deleted;
    /**
     * 节点列表
     */
    @TableField(exist = false)
    private List<Node> nodes;

}
