package com.zhidao.backend.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@TableName("collect")
@Data
public class Collect {

    private Long id;

    @TableField("user_id")
    private Long userId;

    @TableField("route_id")
    private Long routeId;

    @TableLogic
    private Boolean deleted;
}
