package com.zhidao.backend.exception;

import com.zhidao.backend.common.Result;
import com.zhidao.backend.common.StatusCode;
import org.jetbrains.annotations.NotNull;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * 全局异常处理类
 */
@RestControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(RuntimeException.class)
    public Result<?> runtimeExceptionHandler(@NotNull RuntimeException runtimeException) {
        return Result.error(StatusCode.SYSTEM_ERROR.getCode(), runtimeException.getMessage());
    }

    @ExceptionHandler(BusinessException.class)
    public Result<?> businessExceptionHandler(@NotNull BusinessException businessException) {
        return Result.error(businessException.getCode(), businessException.getMessage());
    }
}
