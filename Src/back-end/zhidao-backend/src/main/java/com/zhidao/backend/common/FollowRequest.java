package com.zhidao.backend.common;

import lombok.Data;

@Data
public class FollowRequest {

    private Long bloggerId;

    private Long followerId;
}
