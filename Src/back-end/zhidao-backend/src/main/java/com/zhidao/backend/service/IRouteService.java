package com.zhidao.backend.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zhidao.backend.common.PublishRequest;
import com.zhidao.backend.entity.Route;

import java.util.List;

/**
 * 路线服务
 */
public interface IRouteService extends IService<Route> {

    List<Route> search(String keyword);

    List<Route> getRoutes(Long id);

    List<Route> getDrafts(Long id);

    List<Route> getAuditingRoutes(Long id);

    List<Route> getReturnRoutes(Long id);

    void likeRoute(Long id);

    void collectRoute(Long id);

    Long publishRoute(PublishRequest publishRequest);
    
    void cancelCollectRoute(Long id);

    void unlikeRoute(Long routeId);

    void learnRoute(Long routeId);

    void cancelLearnRoute(Long routeId);

    List<Route> randomRoutes();

    List<Route> personalizedRoutes(String tagStr);

    List<Route> getRoutesById(List<Long> routesId);


    void deleteById(Route route);

    List<Route> getCheckPendingRoutes();

    List<Route> getCheckPassedRoutes();

    List<Route> carouselRoutes();
}
