package com.zhidao.backend.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zhidao.backend.common.CommentToRouteRequest;
import com.zhidao.backend.entity.Comment;

import java.util.List;

public interface ICommentService extends IService<Comment> {
    Long commentToRoute(CommentToRouteRequest commentToRouteRequest);

    List<Comment> getCommentList(Long routeId);

    void deleteComment(Long commentId);

    Comment getCommentById(Long commentId);
}
