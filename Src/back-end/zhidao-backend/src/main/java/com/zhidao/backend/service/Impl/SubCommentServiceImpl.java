package com.zhidao.backend.service.Impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zhidao.backend.common.StatusCode;
import com.zhidao.backend.common.SubCommentRequest;
import com.zhidao.backend.entity.SubComment;
import com.zhidao.backend.exception.BusinessException;
import com.zhidao.backend.mapper.SubCommentMapper;
import com.zhidao.backend.service.ISubCommentService;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class SubCommentServiceImpl extends ServiceImpl<SubCommentMapper, SubComment> implements ISubCommentService {
    @Override
    public Long subCommentAction(@NotNull SubCommentRequest subCommentRequest) {
        Long commentId= subCommentRequest.getCommentId();
        Long commentatorId = subCommentRequest.getCommentatorId();
        Long receiverId=subCommentRequest.getReceiverId();
        String commentLines = subCommentRequest.getCommentLines();
        SubComment subComment = new SubComment();
        subComment.setCommentId(commentId);
        subComment.setCId(commentatorId);
        subComment.setRId(receiverId);
        subComment.setCommentLines(commentLines);
        Date date = new Date();
        Timestamp timestamp = new Timestamp(date.getTime());
        subComment.setCommentTime(timestamp);
        List<SubComment> subCommentList =this.list(new QueryWrapper<SubComment>().eq("comment_id",commentId).orderByDesc("floor"));
        if (subCommentList.size() == 0) {
            subComment.setFloor(1);
        } else {
            Integer maxFloor= subCommentList.get(0).getFloor();
            subComment.setFloor(maxFloor+1);
        }
        int res = baseMapper.insert(subComment);
        if (res < 1) {
            throw new BusinessException(StatusCode.SYSTEM_ERROR.getCode(), "评论失败，请重试");
        }
        return subComment.getId();
    }

    @Override
    public List<SubComment> getSubCommentList(Long commentId) {
        List<SubComment> subCommentList =  baseMapper.selectList(new QueryWrapper<SubComment>().eq("comment_id",commentId).orderByDesc("floor"));
        if (subCommentList == null) {
            return null;
        }
        List<Long> subCommentIdList = new ArrayList<>();
        for (SubComment subComment : subCommentList) {
            subCommentIdList.add(subComment.getId());
        }
        return subCommentList;
    }

    @Override
    public void deleteSubComment(Long subCommentId) {
        int res = baseMapper.deleteById(subCommentId);
        if (res < 1) {
            throw new BusinessException(StatusCode.SYSTEM_ERROR.getCode(), "删除失败，请重试");
        }
    }

    @Override
    public SubComment getSubCommentById(Long subCommentId) {
        SubComment subComment = baseMapper.selectById(subCommentId);
        if (subComment == null) {
            throw new BusinessException(StatusCode.SYSTEM_ERROR.getCode(), "获取失败，请重试");
        }
        return subComment;
    }
}
