package com.zhidao.backend.common;

import lombok.Data;

import java.util.Map;

@Data
public class UpdateRequest {
    private Long id;
    private Map<String, Object> updateAttribute;
}
