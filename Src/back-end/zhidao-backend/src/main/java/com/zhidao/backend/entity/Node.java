package com.zhidao.backend.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.util.List;

@TableName("node")
@Data
public class Node {

    @TableId(type = IdType.AUTO)
    private Long id;

    private String title;

    private String description;

    @TableField("route_id")
    private Long routeId;

    @TableField("position")
    private Integer position;

    @TableLogic
    private Boolean deleted;

    @TableField(exist = false)
    private List<Resource> resources;
}
