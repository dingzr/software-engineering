package com.zhidao.backend.common;

import lombok.Data;
import org.jetbrains.annotations.NotNull;

/**
 * 前端请求的通用响应体
 * @param <T> 存放返回的数据
 */
@Data
public class Result<T> {
    private Integer code;
    private String msg;
    private T data;

    @NotNull
    public static Result success() {
        Result result = new Result<>();
        result.setCode(StatusCode.SUCCESS.getCode());
        result.setMsg("OK");
        return result;
    }

    @NotNull
    public static<T> Result<T> success(T data) {
        Result<T> result = new Result<>();
        result.setCode(StatusCode.SUCCESS.getCode());
        result.setMsg("OK");
        result.setData(data);
        return result;
    }

    @NotNull
    public static Result error(Integer code, String msg) {
        Result result = new Result<>();
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

}
