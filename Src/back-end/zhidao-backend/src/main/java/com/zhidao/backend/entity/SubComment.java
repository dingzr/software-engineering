package com.zhidao.backend.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.sql.Timestamp;

@TableName("subcomment")
@Data
public class SubComment {
    @TableId(type = IdType.AUTO)
    private Long id;
    @TableField("comment_id")
    private Long commentId;
    @TableField("c_id")
    private Long cId;
    @TableField(exist = false)
    private String username;
    @TableField(exist = false)
    private String avatar;
    @TableField("r_id")
    private Long rId;
    private Integer floor;
    @TableField("comment_lines")
    private String commentLines;
    @TableField("comment_time")
    private Timestamp commentTime;
    @TableLogic
    private Boolean deleted;
}
