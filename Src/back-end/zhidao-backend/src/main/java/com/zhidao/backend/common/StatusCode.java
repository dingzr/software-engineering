package com.zhidao.backend.common;

public enum StatusCode {
    SUCCESS(200),
    PARAM_ERROR(400),
    SYSTEM_ERROR(500),
    NOT_LOGIN(601),
    REJECT(403);

    private final Integer code;

    StatusCode(Integer code) {
        this.code = code;
    }

    public Integer getCode() {
        return code;
    }
}
