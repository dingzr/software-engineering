package com.zhidao.backend.common;

import lombok.Data;

@Data
public class UnsealRequest {
    private Long adminId;
    private Long user;
}
