package com.zhidao.backend.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.sql.Timestamp;

@TableName("follow")
@Data
public class Follow {
    /**
     * 主键id
     */
    private Long id;
    /**
     * 被关注者id
     */
    @TableField("blogger_id")
    private Long bloggerId;
    /**
     * 关注者id
     */
    @TableField("follower_id")
    private Long followerId;
    /**
     * 关注时间
     */
    @TableField("follow_time")
    private Timestamp followTime;
    /**
     * 是否删除
     */
    @TableLogic
    private Boolean deleted;
}
