package com.zhidao.backend.common;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

@Data
public class UploadImageRequest {

    private Long id;

    private MultipartFile file;
}
