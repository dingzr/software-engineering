package com.zhidao.backend.common;

import lombok.Data;

@Data
public class ChangePasswordRequest {
    private Long id;
    private String oldPassword;
    private String newPassword;
}
