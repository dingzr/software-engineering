package com.zhidao.backend.service.Impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zhidao.backend.entity.Resource;
import com.zhidao.backend.mapper.ResourceMapper;
import com.zhidao.backend.service.IResourceService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ResourceServiceImpl extends ServiceImpl<ResourceMapper, Resource> implements IResourceService {
    @Override
    public List<Resource> getResourcesByNodeId(Long nodeId) {
        return this.list(new QueryWrapper<Resource>().eq("node_id", nodeId));
    }
}
