package com.zhidao.backend.common;

import lombok.Data;

@Data
public class BanUserRequest {
    private Long banner;
    private Long receiver;
}
