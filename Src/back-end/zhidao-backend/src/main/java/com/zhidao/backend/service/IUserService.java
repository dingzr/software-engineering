package com.zhidao.backend.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zhidao.backend.common.*;
import com.zhidao.backend.entity.User;

import java.util.List;

/**
 * 用户服务
 */
public interface IUserService extends IService<User> {
    void sendBanMail(String to);

    void register(RegisterRequest registerRequest);

    User login(LoginRequest loginRequest);

    void delete(Long id);

    String getUsernameById(Long id);

    User getUserInfo(Long id);

    void updateProfile(UpdateRequest updateRequest);

    void updateAvatar(Long id, String url);

    void existUser(Long id);

    void follow(Long id);

    void beFollowed(Long id);

    List<User> search(String keyword);

    void signIn(Long id);

    void likeRoute(Long id);

    void collectRoute(Long id);
    
    void cancelCollectRoute(Long id);

    void unlikeRoute(Long userId);

    void learnRoute(Long userId);

    void cancelLearnRoute(Long userId);

    void giveAdmin(Long userId);

    Boolean isAdministrator(Long userId);

    void banUser(BanUserRequest banUserRequest);

    void unsealUser(UnsealRequest unsealRequest);

    void changePassword(ChangePasswordRequest changePasswordRequest);

    void unfollow(Long followerId);

    void beUnfollowed(Long bloggerId);
}
