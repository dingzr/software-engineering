package com.zhidao.backend.entity;

import lombok.Data;

@Data
public class DetailRoute {

    private Route route;

    private Boolean liked;

    private Boolean collected;

    private Boolean learned;

    private Integer progress;

}
