package com.zhidao.backend.exception;

public class BusinessException extends RuntimeException{

    private final Integer code;

    public BusinessException(Integer code, String msg) {
        super(msg);
        this.code = code;
    }

    public Integer getCode() {
        return code;
    }
}
