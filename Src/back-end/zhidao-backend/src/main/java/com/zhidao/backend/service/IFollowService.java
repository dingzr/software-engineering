package com.zhidao.backend.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zhidao.backend.common.FollowRequest;
import com.zhidao.backend.entity.Follow;

import java.util.List;

/**
 * 关注服务
 */
public interface IFollowService extends IService<Follow> {

    void follow(FollowRequest followRequest);

    List<Long> getSubscribeList(Long id);

    void unfollow(FollowRequest followRequest);
}
