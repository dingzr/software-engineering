package com.zhidao.backend.common;

import lombok.Data;

@Data
public class SubCommentRequest {
    private Long commentId;

    private Long commentatorId;

    private Long receiverId;

    private String commentLines;
}
