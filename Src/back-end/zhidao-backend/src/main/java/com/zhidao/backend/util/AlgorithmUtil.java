package com.zhidao.backend.util;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class AlgorithmUtil {

    public static int editDistance(@NotNull List<String> list1, @NotNull List<String> list2) {
        int n = list1.size();
        int m = list2.size();
        if (n * m == 0) {
            return (n + m);
        }
        int[][] dp = new int[n + 1][m + 1];
        for (int i = 0; i <= n; i++) {
            dp[i][0] = i;
        }
        for (int j = 0; j <= m; j++) {
            dp[0][j] = j;
        }
        for (int i = 1; i <= n; i++) {
            for (int j = 1; j <= m; j++) {
                if (list1.get(i - 1).equals(list2.get(j - 1))) {
                    dp[i][j] = dp[i - 1][j - 1];
                } else {
                    dp[i][j] = Math.min(dp[i - 1][j - 1], Math.min(dp[i - 1][j], dp[i][j - 1])) + 1;
                }
            }
        }
        return dp[n][m];
    }

    //生成8个给定范围内不同的随机整数
    public static int[] randomArray(int min, int max) {
        int[] arr = new int[max - min + 1];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = min + i;
        }
        for (int i = 0; i < arr.length; i++) {
            int index = (int) (Math.random() * (arr.length - i));
            int temp = arr[index];
            arr[index] = arr[arr.length - i - 1];
            arr[arr.length - i - 1] = temp;
        }
        int[] result = new int[8];
        for (int i = 0; i < 8; i++) {
            result[i] = arr[i];
        }
        return result;
    }
}
