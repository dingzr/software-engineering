package com.zhidao.backend.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.sql.Timestamp;

@TableName("learn")
@Data
public class Learn {

    private Long id;

    @TableField("user_id")
    private Long userId;

    @TableField("route_id")
    private Long routeId;

    private Integer progress;

    @TableField("update_time")
    private Timestamp updateTime;

    @TableLogic
    private Boolean deleted;
}
