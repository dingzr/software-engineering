package com.zhidao.backend.common;

import lombok.Data;

@Data
public class CommentToRouteRequest {
    private Long commentatorId;

    private Long routeId;

    private String commentLines;
}
