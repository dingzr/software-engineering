package com.zhidao.backend.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zhidao.backend.common.SubCommentRequest;
import com.zhidao.backend.entity.Comment;
import com.zhidao.backend.entity.SubComment;

import java.util.List;

public interface ISubCommentService extends IService<SubComment> {
    Long subCommentAction(SubCommentRequest subCommentRequest);

    List<SubComment> getSubCommentList(Long commentId);

    void deleteSubComment(Long subCommentId);

    SubComment getSubCommentById(Long subCommentId);
}
