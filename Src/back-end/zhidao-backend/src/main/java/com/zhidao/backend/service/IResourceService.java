package com.zhidao.backend.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zhidao.backend.entity.Resource;

import java.util.List;

public interface IResourceService extends IService<Resource> {

    List<Resource> getResourcesByNodeId(Long nodeId);
}
