import request from "./request";
// 封装登录请求
export async function login(loginData) {
  return await request.post("/user/login", loginData);
}
// 封装注册请求
export async function register(data) {
  return await request.post("/user/register", data);
}