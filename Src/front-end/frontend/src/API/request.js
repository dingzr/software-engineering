// 引入 axios
import store from "@/store";
import axios from "axios";
import JSONBIG from "json-bigint";
// 封装 baseURL
const request = axios.create({
	baseURL: "http://120.25.227.191:8080",
});

request.defaults.timeout = 7000;
request.defaults.transformResponse = [
	function (data) {
		const json = JSONBIG({
			storeAsString: true,
		});
		return json.parse(data);
	},
];
request.interceptors.request.use(
	(config) => {
		// 判断是否存在token，如果存在的话，则每个http header都加上token
		if (store.state.token) {
			config.headers["Authorization"] = store.state.token;
		}
		return config;
	},
	(error) => {
		return Promise.reject(error);
	}
);

export default request;
// 向外暴露 request
