import request from "./request";
// 封装搜索路线请求
export async function loadRoutes(content) {
  return await request.get(
    "/route/search", {
      params: {
      keyword: content
    }
  }
  );
}
// 封装搜索用户请求
export async function loadUsers(content) {
  return await request.get(
    "/user/search", {
      params: {
      keyword: content
    }
  }
  );
}