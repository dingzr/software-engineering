// 该文件是专门用于创建整个应用的路由器

// 第一步引入插件(本质是一个构造函数)
import VueRouter from "vue-router";

// 引入一下用到的组件
import Manuscripts from "../components/Manager/AuditManuscripts";
import BlackList from "../components/Manager/BlackList";
import UserList from "../components/Manager/UserList";
import CollectInfo from "../components/UserInfo/CollectInfo";
import LearningInfo from "../components/UserInfo/LearningInfo";
import Overview from "../components/UserInfo/Overview";
import PublishInfo from "../components/UserInfo/PublishInfo";
import AboutUs from "../pages/AboutUs";
import EditUser from "../pages/EditUserPage";
import Home from "../pages/HomePage";
import Login from "../pages/LoginPage";
import Manager from "../pages/ManagerPage";
import Register from "../pages/RegisterPage";
import RouteEditor from "../pages/RouteEditorPage";
import Route from "../pages/RoutePage";
import Search from "../pages/SearchPage";
import SearchRoutes from "../pages/SearchRouter/RouteResult";
import SearchUsers from "../pages/SearchRouter/UserResult";
import User from "../pages/UserPage";

import NotFound from "../pages/404Page";

const originalPush = VueRouter.prototype.push;
VueRouter.prototype.push = function push(location) {
	return originalPush.call(this, location).catch((err) => err);
};
// 第二步创建router实例对象并暴露
export default new VueRouter({
	routes: [
		{
			// path是路径
			path: "/",
			//跳转的组件
			component: Home,
		},
		{
			path: "/login",
			component: Login,
		},
		{
			path: "/register",
			component: Register,
		},
		{
			name: "search",
			path: "/search",
			component: Search,
			children: [
				{
					name: "searchRoutes",
					path: "routes",
					component: SearchRoutes,
				},
				{
					path: "users",
					component: SearchUsers,
				},
			],
		},
		{
			path: "/user",
			component: User,
			children: [
				{
					path: "/",
					component: Overview,
				},
				{
					path: "learninginfo",
					component: LearningInfo,
				},
				{
					path: "publishinfo",
					component: PublishInfo,
				},
				{
					path: "collectinfo",
					component: CollectInfo,
				},
			],
		},
		{
			path: "/edit",
			component: EditUser,
		},
		{
			path: "/route",
			component: Route,
		},
		{
			path: "/manager",
			component: Manager,
			children: [
				{
					path: "/",
					component: Manuscripts,
				},
				{
					path: "manuscripts",
					component: Manuscripts,
				},
				{
					path: "userlist",
					component: UserList,
				},
				{
					path: "blacklist",
					component: BlackList,
				},
			],
		},
		{
			path: "/editor",
			component: RouteEditor,
		},
		{
			path: "/about",
			component: AboutUs,
		},
		{
			path: "/404",
			component: NotFound,
		},
		{
			path: "*",
			redirect: "/404",
		},
	],
});
// 然后去main.js中引入router实例
