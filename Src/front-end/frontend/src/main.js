import VMdPreview from "@kangc/v-md-editor/lib/preview";
import "@kangc/v-md-editor/lib/style/preview.css";
import githubTheme from "@kangc/v-md-editor/lib/theme/github.js";
import "@kangc/v-md-editor/lib/theme/style/github.css";
import Antd from "ant-design-vue";
import "ant-design-vue/dist/antd.css";
import ElementUI from "element-ui";
import "element-ui/lib/theme-chalk/index.css";
import hljs from "highlight.js";
import mavonEditor from "mavon-editor";
import "mavon-editor/dist/css/index.css";
import Vue from "vue";
import VueRouter from "vue-router";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import { Loading } from "element-ui";
VMdPreview.use(githubTheme, {
	Hljs: hljs,
});
Vue.use(hljs);

Vue.use(ElementUI);
Vue.use(Antd);
Vue.use(VueRouter);
Vue.use(mavonEditor);
Vue.use(VMdPreview);
Vue.use(Loading.directive);

Vue.config.productionTip = false;

new Vue({
	render: (h) => h(App),
	beforeCreate() {
		Vue.prototype.$bus = this;
	},
	router,
	store,
}).$mount("#app");
