# “知道”平台 - 软件工程项目
### 文件组织结构
Doc目录存放图片、wiki文档以及课程资料等。 Src目录存放项目代码。
  
### 软件简介
PC端网页平台-“知道”。从0开始，指点你的学习路线。
  
### 小组成员

|  登录名  |  姓名  |  分工  |  贡献  |
|  ---  |  ---  |  :--  |  ---  |
|  娄志远  |  娄志远  |后端、主要文档编写、项目管理|25%|
|  KenGuo  |  郭晨  |后端、数据库搭建、前端、测试|25%|
|  丁兆睿  |  丁兆睿  |前端|25%|
|  茝彧  |  李孟阳  |前端|25%|  

